//
//  TransactionsViewController.swift
//  Transactions
//
//  Created by Raluca Bujor on 28/02/2019.
//  Copyright © 2019 Raluca Bujor. All rights reserved.
//

import UIKit
import SwiftGraph

class TransactionsViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalSumLabel: UILabel!
    
    //MARK: Variables
    var product: String!
    var transactions: [Transaction]!
    var filteredTransactions: [Transaction] = []
    var ratesArray: [Rate]!
    var ratesCurrency: [String] = []
    var rates: WeightedGraph<String, Int>!
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.topItem?.title = "\(String(describing: product!)) Transactions"
        configureRates()
        setDelegates()
        filterTransactions()
    }
    
    //MARK: Helpers
    func configureRates() {
        for rate in ratesArray {
            ratesCurrency.append("\(rate.from)")
            ratesCurrency.append("\(rate.to)")
            print("\(rate.from),\(rate.to) -> \(rate.rate)")
        }
        
        rates = WeightedGraph<String, Int>(vertices: ratesCurrency)
        
        for rate in ratesArray {
            rates.addEdge(from: rate.from, to: rate.to, weight: Int(rate.rate * 100), directed: true)
        }
    }
    
    func setDelegates() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func filterTransactions() {
        filteredTransactions = transactions.filter({$0.product == product})
        var amountInEuros = 0.0
        for transaction in filteredTransactions {
            if transaction.currency == "EUR" {
                amountInEuros = aproximateValue(amountInEuros + aproximateValue(transaction.amount))
            } else {
                amountInEuros = aproximateValue((amountInEuros + aproximateValue(aproximateValue(transaction.amount) * aproximateValue(getAmountInEuros(from: transaction.currency)))))
            }
            
        }
        self.totalSumLabel.text = "Total Sum: \(aproximateValue(amountInEuros)) EUR"
        tableView.reloadData()
    }
    
    func getAmountInEuros(from: String) -> Double {
        var totalAmount: Double = 1
        let (_, pathDict) = rates.dijkstra(root: from, startDistance: 0)
        
        let path: [WeightedEdge<Int>] = pathDictToPath(from: rates.indexOfVertex(from)!, to: rates.indexOfVertex("EUR")!, pathDict: pathDict)
        let stops: [String] = rates.edgesToVertices(edges: path)
        
        
        for i in 0..<(stops.count - 1) {
            if let value = ratesArray.first(where: {$0.from == stops[i] && $0.to == stops[i+1]}) {
                totalAmount = totalAmount * value.rate
            }
        }
        return totalAmount
    }
    
    func aproximateValue(_ value: Double) -> Double {
        var newValue = Int(value * 1000)
        newValue = newValue%10 >= 5 ? (newValue + 10):(newValue)
        newValue = newValue/10
        return Double(newValue)/100
    }
}

extension TransactionsViewController: UITableViewDataSource, UITableViewDelegate {
    
    //TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredTransactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "transactionsTableViewCell", for: indexPath) as! TransactionsTableViewCell
        cell.setUpCell(amount: filteredTransactions[indexPath.row].amount, currency: filteredTransactions[indexPath.row].currency)
        
        return cell
    }
    
}

