//
//  ViewController.swift
//  Transactions
//
//  Created by Raluca Bujor on 28/02/2019.
//  Copyright © 2019 Raluca Bujor. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: Variables
    var rates: [Rate] = []
    var transactions: [Transaction] = []
    var products: [String] = []
    var indexForCell: Int = 0
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDelegates()
        getRates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.topItem?.title = "Products"
    }
    
    //MARK: Helpers
    func setDelegates() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func getRates() {
        APIRequests.getRates { (result) in
            if let result = result {
                self.rates = result
                self.getTransactions()
            } else {
                let alert = UIAlertController(title: nil, message: "Coult not load rates", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func getTransactions() {
        APIRequests.getTransactions { (result) in
            if let result = result {
                self.transactions = result
                self.getProducts()
            } else {
                let alert = UIAlertController(title: nil, message: "Coult not load transactions", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func getProducts() {
        for transaction in transactions {
            if !products.contains(transaction.product) {
                products.append(transaction.product)
            }
        }
        tableView.reloadData()
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "productTransactionsSegue", let destinationVC = segue.destination as? TransactionsViewController {
            destinationVC.product = products[indexForCell]
            destinationVC.transactions = transactions
            destinationVC.ratesArray = rates
        }
    }
}

extension ProductsViewController: UITableViewDelegate, UITableViewDataSource {
    
    //TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productsTableViewCell", for: indexPath) as! ProductsTableViewCell
        
        cell.nameLabel.text = products[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.indexForCell = indexPath.row
        self.performSegue(withIdentifier: "productTransactionsSegue", sender: self)
    }
    
}
