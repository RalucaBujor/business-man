//
//  APIRequest.swift
//  Transactions
//
//  Created by Raluca Bujor on 28/02/2019.
//  Copyright © 2019 Raluca Bujor. All rights reserved.
//

import Alamofire
import UIKit
import SwiftyJSON

class APIRequests {
    
    static func generateJSON(
        for path: String,
        callback:@escaping (_ result: [JSON]?, _ error: Bool) -> ()
        ){
        if let url = URL(string: path){
            Alamofire.request(url)
                .responseJSON{
                    response in
                    switch response.result{
                    case .failure( _):
                        callback(nil, true)
                    case .success(let value):
                        if let responseArray = JSON(value).array {
                            callback(responseArray, false)
                        }
                    }
            }
        }
    }
    
    static func getRates(_ callback: @escaping(_ conversions: [Rate]?) -> ()){
        self.generateJSON(for: "http://gnb.dev.airtouchmedia.com/rates.json", callback: {
            (result: [JSON]?, failed: Bool) -> Void in
            if failed{
                callback(nil)
            }else{
                var rates: [Rate] = []
                for rate in result! {
                    if let rateDecoded = Rate.decode(rate) {
                        rates.append(rateDecoded)
                    }
                }
                callback(rates)
            }
        })
    }
    
    static func getTransactions(_ callback: @escaping(_ conversions: [Transaction]?) -> ()){
        self.generateJSON(for: "http://gnb.dev.airtouchmedia.com/transactions.json", callback: {
            (result: [JSON]?, failed: Bool) -> Void in
            if failed{
                callback(nil)
            }else{
                var transactions: [Transaction] = []
                for transaction in result! {
                    if let transactionDecoded = Transaction.decode(transaction) {
                        transactions.append(transactionDecoded)
                    }
                }
                callback(transactions)
            }
        })
    }
    
}
