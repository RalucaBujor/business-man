//
//  TransactionsTableViewCell.swift
//  Transactions
//
//  Created by Raluca Bujor on 28/02/2019.
//  Copyright © 2019 Raluca Bujor. All rights reserved.
//

import UIKit

class TransactionsTableViewCell: UITableViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    
    //MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: Helpers
    func setUpCell(amount: Double, currency: String) {
        self.amountLabel.text = "\(amount)"
        self.currencyLabel.text = "\(currency)"
    }
}

