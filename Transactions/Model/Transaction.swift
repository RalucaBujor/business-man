//
//  Transaction.swift
//  Transactions
//
//  Created by Raluca Bujor on 28/02/2019.
//  Copyright © 2019 Raluca Bujor. All rights reserved.
//

import UIKit
import SwiftyJSON

class Transaction {
    var product: String
    var amount: Double
    var currency: String
    
    init(product: String, amount: Double, currency: String) {
        self.product = product
        self.amount = amount
        self.currency = currency
    }
    
    static func decode(_ json: JSON) -> Transaction? {
        
        guard let product = json["sku"].string,
            let amount = json["amount"].string,
            let currency = json["currency"].string
            else{
                return nil
        }
        
        if let amount = Double(amount) {
            return Transaction(product: product, amount: amount, currency: currency)
        }
        return Transaction(product: product, amount: 0.0, currency: currency)
    }
}

