//
//  Rate.swift
//  Transactions
//
//  Created by Raluca Bujor on 28/02/2019.
//  Copyright © 2019 Raluca Bujor. All rights reserved.
//

import UIKit
import SwiftyJSON

class Rate {
    var from: String
    var to: String
    var rate: Double
    
    init(from: String, to: String, rate: Double) {
        self.from = from
        self.to = to
        self.rate = rate
    }
    
    static func decode(_ json: JSON) -> Rate? {
        
        guard let from = json["from"].string,
            let to = json["to"].string,
            let rate = json["rate"].string
            else{
                return nil
        }
        
        if let doubleRate = Double(rate) {
            return Rate(from: from, to: to, rate: doubleRate)
        }
        return Rate(from: from, to: to, rate: 0.0)
    }
}
